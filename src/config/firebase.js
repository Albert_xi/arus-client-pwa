var firebase = require("firebase");
var firebaseConfig = {
  apiKey: "AIzaSyC8m7zQ3NGXKD7YS9WJyAy_VP_DmjDEg9A",
  authDomain: "arus-app-reactjs-starterkit.firebaseapp.com",
  databaseURL: "https://arus-app-reactjs-starterkit.firebaseio.com",
  projectId: "arus-app-reactjs-starterkit",
  storageBucket: "arus-app-reactjs-starterkit.appspot.com",
  messagingSenderId: "563265190718",
  appId: "1:563265190718:web:2ff8f9c4efb08c7c0c8a6f",
  measurementId: "G-PTGNCXZ44S"
};

const fire = firebase.initializeApp(firebaseConfig);

export default fire;
