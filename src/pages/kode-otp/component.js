import React, { useState } from "react";
import { Container, Typography } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { Toolbar } from "@material-ui/core";
import LogoOtp from "../../assets/kode-otp.png";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import useStyles from "./style";

function SplashScreen(props) {
  const classes = useStyles();
  const [otp, setOtp] = useState("");
  // const phone = JSON.parse(localStorage.getItem("phone"));

  const handleChange = () => {
    setOtp({ otp });
  };
  const handleKodeOTP = () => {
    props.history.push("/otp/kode-otp");
  };

  const handleBack = () => {
    props.history.push("/login/otp");
  };
  return (
    <Container maxWidth="xs" className={classes.Container}>
      <Grid container>
        <AppBar
          position="static"
          className={classes.appbar}
          onClick={handleBack}
        >
          <Toolbar>
            <ArrowBackIcon />
          </Toolbar>
        </AppBar>
        <img src={LogoOtp} alt="login otp" className={classes.otp} />
      </Grid>

      <Grid container spacing={0}>
        <Grid item xs={12} className={classes.gridText}>
          <Typography className={classes.text}>Kode OTP</Typography>
          <Typography className={classes.kode}>
            Masukkan Kode OTP yang dikirim ke <br />
            <Typography className={classes.number}>
              +62 859-0910-0912
            </Typography>
          </Typography>
        </Grid>

        <Grid item xs={12} className={classes.gridGanti}>
          <Typography className={classes.gantiNomor} onClick={handleBack}>
            Ganti Nomor
          </Typography>
        </Grid>

        <Grid item xs={12} className={classes.inputGrid}>
          <TextField
            align="center"
            placeholder="Kode OTP"
            type="number"
            fullWidth={true}
            style={{ width: "90%" }}
            margin="normal"
          />
        </Grid>
      </Grid>

      <Grid container spacing={0} className={classes.gridButton}>
        <Button
          disableRipple={true}
          id="submit-button"
          className={classes.buttonBottom}
          style={{
            backgroundColor: "#F7A647"
          }}
          onClick={handleKodeOTP}
        >
          Lanjutkan
        </Button>
      </Grid>

      <Grid item align="center" className={classes.gridAkhir}>
        <Typography>
          <b>Kirim Ulang?</b> &nbsp; 00:30
        </Typography>
      </Grid>
    </Container>
  );
}

export default SplashScreen;
