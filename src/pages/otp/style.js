import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  Container: {
    width: "100%",
    height: "100vh",
    padding: 0
  },
  appbar: {
    backgroundColor: "#26CAC0",
    width: "100%",
    position: "fixed",
    top: 0,
    maxWidth: 448,
    display: "flex",
    justifyContent: "center",
    boxShadow: "none"
  },
  otp: {
    width: "100%",
    maxWidth: 448,
    display: "flex",
    justifyContent: "center"
  },
  gridText: {
    paddingLeft: "4%",
    paddingTop: "5%"
  },
  text: {
    fontSize: "24px",
    fontWeight: "bold",
    color: "#25282B"
  },
  kode: {
    fontSize: "12px",
    paddingTop: "5%",
    color: "#9E9E9E"
  },
  inputGrid: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: "8%"
  },
  gridButton: {
    display: "flex",
    justifyContent: "center"
  },
  buttonBottom: {
    fontSize: "15px",
    width: "90%",
    textTransform: "none",
    color: "#FFFFFF",
    bottom: "-20px",
    height: 48,
    boxShadow: "0px 0px 2px #c1b1b1",
    border: "1px solid #F7A647",
    borderRadius: "4px"
  }
}));
export default useStyles;
