import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles(theme => ({
  container: {
    width: "100%",
    padding: 0,
    backgroundSize: "cover"
  },
  gridOne: {
    paddingTop: "20%"
  },
  search: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  box: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    border: "1px solid #9E9E9E",
    width: "90%",
    height: 48,
    borderRadius: "8px",
    boxShadow: "0px 0px 2px #c1b1b1"
  },
  gridTop: {
    backgroundColor: "#FFFFFF"
  },
  gridTabs: {
    marginTop: "3%"
  },
  button: {
    fontFamily: "Montserrat, sans-serif",
    width: "90%",
    height: 48,
    boxShadow: "0px 0px 2px #c1b1b1",
    border: "1px solid #F7A647",
    backgroundColor: "#ffffff",
    borderRadius: "4px",
    color: "#F7A647",
    fontSize: "14px"
  },
  buttonBox: {
    flexDirection: "column",
    display: "flex",
    alignItems: "center"
  },
  gridButton: {
    display: "flex",
    justifyContent: "center"
  },
  Tabs: {
    textTransform: "none"
  },
  gridItemOne: {
    border: "0.9px solid #9E9E9E",
    width: "94%",
    borderRadius: "8px"
  },
  gridField: {
    display: "flex",
    justifyContent: "flex-end",
    paddingTop: 10,
    paddingBottom: 0,
    paddingLeft: "1em",
    fontSize: 14
  },
  TextField: {
    textDecoration: "none"
  },
  gridIcon: {
    display: "flex",
    justifyContent: "flex-end",
    paddingTop: 2,
    paddingBottom: 3
  },
  image: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "10%"
  },
  text: {
    color: "#9E9E9E",
    fontSize: "14px",
    marginTop: "2em"
  },
  boxButton: {
    position: "fixed",
    bottom: "13%",
    display: "flex",
    justifyContent: "center",
    width: "100%",
    maxWidth: "448px"
  },
  gridRiwayat: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: "3%"
  },
  textNumber: {
    color: "#26CAC0",
    display: "flex"
  },
  loading: {
    margin: "45%"
  }
}));
export default useStyles;
